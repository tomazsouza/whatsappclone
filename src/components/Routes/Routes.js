import React, {Component} from 'react';
import {Router, Scene, Stack} from 'react-native-router-flux';

import FormRegister from './components/FormRegister';
import Login from './components/Login';

class Routes extends Component {
  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene key="login" component={Login} title="Login" initil />
          <Scene key="formRegister" component={FormRegister} title="Cadastro" />
        </Stack>
      </Router>
    );
  }
}

export default Routes;
