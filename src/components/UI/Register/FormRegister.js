import React, {Component} from 'react';
import {Text, View, TextInput, Button} from 'react-native';

import styles from './Styles';

class FormRegister extends Component {
  render () {
    return (
      <View style={{flex: 1, padding: 10}}>
        <View style={{flex: 4, justifyContent: 'center'}}>
          <TextInput placeholder={'Nome'} style={styles.textInput} />
          <TextInput placeholder={'E-mail'} style={styles.textInput} />
          <TextInput placeholder={'Senha'} style={styles.textInput} />
        </View>
        <View>
          <Button title="CADASTRAR-SE" color="#115E54" onPress={() => false} />
        </View>
      </View>
    );
  }
}

export default FormRegister;
