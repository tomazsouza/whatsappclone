import {StyleSheet} from 'react-native';

export default StyleSheet.create ({
  textInput: {
    fontSize: 20,
    height: 45,
    borderColor: '#ccc',
    borderWidth: 1,
    margin: 8,
  },
});
