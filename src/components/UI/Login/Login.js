import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Button,
  Text,
} from 'react-native';

import {Actions} from 'react-native-router-flux';

const styles = StyleSheet.create({
  textInput: {
    height: 45,
    fontSize: 20,
    borderColor: '#ccc',
    borderWidth: 1,
    margin: 8,
  },
  container: {
    padding: 10,
    flex: 1,
  },
  title: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTexts: {
    flex: 2,
    fontSize: 16,
  },
  button: {
    padding: 8,
    color: '#FF00FF',
  },
});

function buttonClick() {}

class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text>WhatsApp Clone</Text>
        </View>
        <View style={{flex: 2}}>
          <TextInput style={styles.textInput} placeholder={'E-mail'} />
          <TextInput style={styles.textInput} placeholder={'Senha'} />

          <TouchableOpacity
            onPress={() => Actions.formRegister()}
            style={styles.button}
          >
            <Text>Ainda não tem cadastro? Cadastre-se</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 2}}>
          <Button
            title={'LOGIN'}
            color="#115E54"
            onPress={() => buttonClick()}
          />
        </View>
      </View>
    );
  }
}

export default Login;
